// Vidljivi objekti:
// userData - mapa s podatcima koji se perzistiraju na poslužitelju
// questionCompleted - booleova zastavica

// metoda koja se poziva pri inicijalizaciji pitanja; u userData se može upisati sve što je potrebno...
function questionInitialize() {
  userData.elems = [/*@#import#(pattern${iid}.txt)*/];
  userData.expectedInput = [1,0,1,0,1,0,0,0];
  userData.userInput = [-1,-1,-1,-1,-1,-1,-1,-1];
}

// metoda koja se poziva kako bi napravila prikaz potrebne stranice; dobiva kao argument pogled koji je zadao korisnik
function questionRenderView(vid,infoParams) {
  if(vid=="start") {
    res = {};
    res.options = ['cont'];
    res.questionState = JSON.stringify({elems: userData.elems, userInput: userData.userInput});
    res.view = {action: "page:1"};
    return res;
  }
  if(vid=="done") {
    return {
      options: ['cont'],
      questionState: JSON.stringify({}),
      view: {action: "page:4"}
    };
  }
  if(vid=="no-good") {
    var vars = {};
    vars.totalIncorrect = infoParams.inv;
    if(infoParams.er!=-1) {
      vars.row = infoParams.er;
      vars.error = true;
    } else {
      vars.row = infoParams.em;
      vars.error = false;
    }
    vars.x2 = userData.elems[vars.row][0];
    vars.x1 = userData.elems[vars.row][1];
    vars.x0 = userData.elems[vars.row][2];
    vars.n = vars.row;
    vars.func = vars.row*vars.row+3;
    return {
      options: ['ret'],
      questionState: JSON.stringify({}),
      view: {action: "page:2"},
      variables: vars
    };
  }
  return null;
}

// metoda koja se poziva kako bi se obradio poslan odgovor; prima kljuc (tj. opciju koja je pritisnuta) i objekt s poslanim podatcima
function questionProcessKey(vid, key, sentData) {
  if(vid=="start") {
    userData.userInput = sentData.userInput;
    var firstError = -1;
    var firstEmpty = -1;
    var totalInvalid = 0;
    for(var i = 0; i < userData.expectedInput.length; i++) {
      if(userData.expectedInput[i] != userData.userInput[i]) {
        totalInvalid++;
        if(userData.userInput[i]==-1 && firstEmpty==-1) firstEmpty=i;
        if(userData.userInput[i]!=-1 && firstError==-1) firstError=i;
      }
    }
    if(firstError==-1 && firstEmpty==-1) {
      return {action: "view:done", completed: true};
    } else {
      return {action: "view:no-good", info: {er: firstError, em: firstEmpty, inv: totalInvalid}};
    }
  } else if(vid=="done") {
    if(!questionCompleted) {
      return {action: "view:start"};
    } else {
      return {action: "next"};
    }
  } else if(vid=="no-good") {
    return {action: "view:start"};
  } else {
    return {action: "fail"};
  }
}

