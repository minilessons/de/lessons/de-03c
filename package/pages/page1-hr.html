
<p>Booleova funkcija je temeljni pojam pri proučavanju digitalnog sklopovlja. Booleove funkcije koristit ćemo za opisivanje, analiziranje, transformiranje i sintezu digitalnog sklopovlja. No da bismo to mogli, naprije se moramo upoznati s pojmom Booleove funkcije.</p>

<p>Sjetite se srednješkolske matematike. Što je bila funkcija? Svi smo naučili: <em>funkcija je preslikavanje elemenata domene u elemente kodomene</em>. To preslikavanje (a time i sama funkcija) može biti injektivno, surjektivno te (ako je oboje, onda kažemo) bijektivno. Kod takvog pogleda na funkciju morali smo na neki način definirati što je njezina domena, što kodomena te kako izgleda to preslikavanje.</p>

<p><b>Primjer 1.</b> Neka je funkcija \( f(x) = x+1 \) definirana nad skupom cijelih brojeva \(\mathbb{Z}\). Njezina domena je \(\mathbb{Z}\). Njezina kodomena je također \(\mathbb{Z}\). Ta je funkcija bijekcija, a preslikavanje je zadano izrazom \( x \rightarrow x+1 \): element domene 0 preslikava se u element kodomene 1, element domene 1 preslikava se u element kodomene 2, i tako dalje.</p>

<p><b>Primjer 2.</b> Neka je funkcija \( f(x,y) = \text{"euklidska udaljenost točke }(x,y)\text{ od ishodišta"} \), gdje su \(x \in \mathbb{Z}\) i \(y \in \mathbb{Z}\); drugim riječima, \((x,y)\) su točke koje biramo po cjelobrojnoj rešetki. Domena ove funkcije je \(\mathbb{Z} \times \mathbb{Z}\) (Kartezijev produkt), što kraće pišemo \(\mathbb{Z}^2\). Kodomena funkcije je skup realnih brojeva \(\mathbb{R}\) (dapače, podskup tog skupa jer udaljenosti za početak ne mogu biti negativni brojevi). Matematičkim riječnikom rekli bismo da imamo \(f: \mathbb{Z}^2 \rightarrow \mathbb{R}\), pri čemu je samo preslikavanje definirano izrazom \( (x,y) \rightarrow \sqrt{x^2+y^2} \). Pogledajte što su sada elementi domene. To su uređeni parovi (uređene dvojke) cijelih brojeva. Elementi kodomene su realni brojevi. Zadana funkcija preslikava element domene (0,0) u element kodomene 0, element domene (0,1) u element kodomene 1, element domene (1,1) u element kodomene \(\sqrt{2}\), i tako dalje.</p>

<p>Booleove funkcije u ovom smislu nisu ništa posebno; one su funkcije (dakle preslikavanje) iz domene u kodomenu. Pojam "Booleova" pri tome označava da su domena i kodomena jednog specifičnog oblika. Prisjetite se kako smo definirali Booleovu algebru i što smo zaključili da danas podrazumijevamo pod tim nazivom: krenuli smo od nekog općenitog skupa \(\mathbb{K}\) za koji smo tražili da ima barem dva različita elementa, te smo nad njim definirali dva binarna operatora, jedan unarni i zatim šest postulata koji vrijede. Potom smo zaključili da uzevši u obzir činjenicu da su naša računala binarna (pamte nule i jedinice) i da su izgrađena od (danas elektroničkih) sklopki koje mogu biti uključene i isključene, upravo je minimalni dvočlani skup ono što nam je potrebno za opisivanje digitalnog sklopovlja. Taj dvočlani skup od sada ćemo označavati slovom \(\mathbb{B}\), pa možemo pisati \(\mathbb{B}=\{0,1\}\). Element 0 u logičkom smislu ćemo poistovjećivati i s neistinom (engl. <i>false</i>) a element 1 s istinom (engl. <i>true</i>), pa bismo jednako tako mogli pisati \(\mathbb{B}=\{false, true\}\), ili ako ste u srednjoj školi slušali o logikama, čak i \(\mathbb{B}=\{\bot, \top\}\). Međutim, u inženjerskoj praksi, zbog činjenice da se 0 i 1 često koriste i kao binarni brojevi, uobičajen je prvi način pisanja i mi ćemo ga dalje koristiti.</p>

<div class="mlImportant">
<p>Uz prethodno dogovorenu definiciju skupa \(\mathbb{B}\), <em>Booleovu funkciju</em> definirat ćemo kao preslikavanje iz domene \(\mathbb{B}^k\) u domenu \(\mathbb{B}\) pri čemu je <i>k</i> broj argumenata te funkcije (kažemo još da je funkcija definirana nad <i>k</i> Booleovih - ili binarnih - varijabli). Elementi domene takve funkcije su uređene <i>k</i>-torke koje ćemo u praksi zapisivati kao niz od <i>k</i> bitova.</p>
<p>Još općenitija definicija <em>Booleove funkcije</em> bila bi preslikavanje iz domene \(\mathbb{B}^k\) u domenu \(\mathbb{B}^l\). Međutim, s takvim Booleovim funkcijama upoznat ćemo se nešto kasnije, i zvat ćemo ih <em>višeizlazne Booleove funkcije</em>.</p>
</div>

<p>Ponekad argumenti funkcije neće imati nikakvu dodatnu semantiku. Primjerice, razmotrimo Booleovu funkciju \(f(x,y) = x \oplus y\) (dakle isključivo-ILI od <i>x</i> i <i>y</i>). Njezina domena je \(\mathbb{B}^2\) a kodomena \(\mathbb{B}\). Preslikavanje koje ova funkcija predstavlja možemo prikazati na nekoliko načina. Grafički prikaz preslikavanja uporabom strelica prikazan je na slikama u nastavku. Pri tome se na lijevoj slici elementi domene prikazuju kao uređeni parovi dok se na desnoj slici koristi prikaz nizom bitova.
</p>

<table class="mlTableCC">
<tr><td>
<img src="@#file#(fand1.svg)" alt="Elementi domene prikazani kao uređeni parovi"><br>
a) Elementi domene prikazani su kao uređeni parovi (<i>x</i>,<i>y</i>)
</td><td>
<img src="@#file#(fand2.svg)" alt="Elementi domene prikazani kao niz bitova"><br>
b) Elementi domene prikazani su kao niz binarnih znamenki (<i>xy</i>)
</td></tr>
</table>

<p>Umjesto ovakvih prikaza, u inženjerskoj praksi najčešće se koristi prikaz tablicom istinitosti. Takav prikaz za ovu je funkciju dan u nastavku.</p>

<div class="mlTableCaption"><span class="mlTblKey">Tablica 1.</span> Tablica istinitosti za funkciju \(f(x,y)\).</div>
<table class="mlTableCC mlTableVR2">
<thead><tr><th>\(x\)</th><th>\(y\)</th><th>\(f(x,y)\)</th></tr></thead>
<tbody>
<tr><td>0</td><td>0</td><td>0</td></tr>
<tr><td>0</td><td>1</td><td>1</td></tr>
<tr><td>1</td><td>0</td><td>1</td></tr>
<tr><td>1</td><td>1</td><td>0</td></tr>
</tbody>
</table>

<p>Prokomentirajmo malo danu tablicu. Tablica prikazuje preslikavanje s \(f: \mathbb{B}^2 \rightarrow \mathbb{B}\), odnosno predstavlja funkciju \(f(x,y)\). Kako se radi o funkciji od dvije varijable, postoje ukupno četiri različita uređena para koji predstavljaju sve moguće kombinacije koje mogu poprimiti varijable <i>x</i> i <i>y</i>. Općenito, funkcija od <i>n</i> varijabli ima \(2^n\) različitih binarnih <i>n</i>-torki pa bi upravo toliko redaka imala i tablica istinitosti.
</p>

<p>Tablica se sastoji od dva dijela: prvi dio čine prva dva stupca: stupci varijabli o kojima ovisi funkcija. Taj dio tablice navodi sve elemente domene funkcije. Drugi dio tablice (treći odnosno posljednji stupac) definira samo preslikavanje: za svaki element domene ovdje je zapisano u koji se element kodomene on preslikava. Ova dva dijela i vizualno su razdvojena okomitom linijom.</p>

<p>Prilikom popunjavanja prvog dijela tablice, uobičajena je praksa <i>n</i>-torke nizati počevši od svih nula na način da svaki sljedeći redak predstavlja za jedan veći cijeli broj, ako se binarni uzorak tumači kao binarno kodirani cijeli broj. Na ovo ćemo se osloniti kod postupaka koji retke tablice numeriraju i dalje rade s indeksima; stoga nemojte tablice popunjavati drugačije.</p>

<div class="mlImportant">
<p>Digitalni sklopovi koji ostvaruju Booleove funkcije nazivaju se <em>kombinacijski digitalni sklopovi</em> (engleski termin je <i>combinatorial digital circuits</i>).</p>
<p>Osnovna karakteristika kombinacijskog sklopovlja jest da se "ponaša" u skladu s matematičkom definicijom funkcije: sklop ostvaruje zadano preslikavanje pa svaki puta kada mu se na ulaz dovede određena kombinacija binarnih vrijednosti (određena <i>n</i>-torka), sklop generira izlaz koji je određen preslikavanjem koje sklop obavlja.</p>
</div>

<p>Dvoulazni digitalni sklopovi koji na izlazu generiraju vrijednost koja je jednaka primjeni operatora logičko-I, logičko-ILI ili logičko-isključivo-ILI na vrijednosti dovedene na ulaze sklopa primjer su kombinacijskih sklopova.</p>

