
<p>Koliko postoji različitih Booleovih funkcija? Do odgovora na ovo pitanje lagano ćemo doći ako se prisjetimo kako se specificira Booleova funkcija. Booleova funkcija u cijelosti je specificirana ako za svaki element domene znamo u koji ga element kodomene funkcija preslikava. Ako imamo Booleovu funkciju oblika \(f: \mathbb{B}^k \rightarrow \mathbb{B}\) čiji su elementi domene uređene <i>k</i>-torke nula i jedinica (ili binarni nizovi duljine <i>k</i>), tada znamo da je broj elemenata u domeni (odnosno njezin kardinalitet) jednak \(2^k\). Tako primjerice Booleova funkcija definirana nad jednom varijablom, odnosno \(f: \mathbb{B} \rightarrow \mathbb{B}\), ima u domeni samo \(2^1=2\) elementa: {0, 1}, Booleova funkcija definirana nad dvije varijable, odnosno \(f: \mathbb{B}^2 \rightarrow \mathbb{B}\), ima u domeni \(2^2=4\) elementa: {00, 01, 10, 11}, Booleova funkcija definirana nad tri varijable, odnosno \(f: \mathbb{B}^3 \rightarrow \mathbb{B}\) ima u domeni \(2^3=8\) elemenata: {000, 001, 010, 011, 100, 101, 110, 111}, i tako dalje.</p>

<p>Za svaki od tih elemenata domene, Booleova funkcija isti može preslikati ili u element 0 ili u element 1 kodomene. Stoga prvi element domene možemo preslikati na dva načina, drugi element domene na dva načina, treći element domene na dva načina, ..., sve do posljednjeg, \(2^k\)-tog elementa domene kojeg također možemo preslikati na dva načina. Broj različitih načina na koje možemo preslikati svih \(2^k\) elemenata jednak je broju različitih Booleovih funkcija nad <i>k</i> varijabli i odgovara:
$$
 \underbrace{2 \cdot 2 \cdot \cdots \cdot 2}_{2^k} = 2^{2^k},
$$
što je broj koji raste ekstremno brzo. Tablica u nastavku daje broj različitih Booleovih funkcija ovisno o broju varijabli (tj. argumenata) funkcije.
</p>

<div class="mlTableCaption"><span class="mlTblKey">Tablica 1.</span> Broj Booleovih funkcija ovisno o broju varijabli.</div>
<table class="mlTableCC">
<thead>
<tr><th>Broj varijabli</th><th>Broj Booleovih funkcija</th></tr>
</thead>
<tbody>
<tr><td>0</td><td>2</td></tr>
<tr><td>1</td><td>4</td></tr>
<tr><td>2</td><td>16</td></tr>
<tr><td>3</td><td>256</td></tr>
<tr><td>4</td><td>65536</td></tr>
<tr><td>5</td><td>\( 4294967296 = \; \approx 4,3 \cdot 10^{9}\)</td></tr>
<tr><td>6</td><td>\(\approx 1,84 \cdot 10^{19}\)</td></tr>
<tr><td>7</td><td>\(\approx 3,4 \cdot 10^{38}\)</td></tr>
<tr><td>8</td><td>\(\approx 1,16 \cdot 10^{77}\)</td></tr>
</tbody>
</table>

<p>Pogledajmo koje bi sve bile Booleove funkcije od jedne varijable.</p>

<div class="mlTableCaption"><span class="mlTblKey">Tablica 2.</span> Sve Booleove funkcije od jedne varijable.</div>
<table class="mlTableCC mlTableVRl1 mlTableVRl2 mlTableVRl3 mlTableVRl4">
<thead><tr><th>\(x\)</th><th>\(f_0\)</th><th>\(f_1\)</th><th>\(f_2\)</th><th>\(f_3\)</th></tr></thead>
<tbody>
<tr><td>0</td><td>0</td><td>0</td><td>1</td><td>1</td></tr>
<tr><td>1</td><td>0</td><td>1</td><td>0</td><td>1</td></tr>
</tbody>
</table>

<p>Među mnoštvom različitih Booleovih funkcija potrebno je istaknuti nekoliko važnih porodica funkcija. Krenut ćemo s dvije temeljne: kontradikcijom i tautologijom.</p>

<div class="mlImportant"><b>Kontradikcija</b><br>
Booleova funkcija \(f: \mathbb{B}^k \rightarrow \mathbb{B}\) koja svaki element domene preslikava u 0 naziva se <em>kontradikcija</em>. Tablica istinitosti ove funkcije u posljednjem stupcu ima sve vrijednosti postavljene na 0. Na funkciju možemo gledati i ovako: to je funkcija čija vrijednost ne ovisi o vrijednostima njezinih argumenata - ona je uvijek 0.
</div>

<p>U klasičnoj logici, kontradikciju još zovemo i nezadovoljivom funkcijom: ne postoji niti jedna kombinacija ulaza koja bi zadovoljila funkciju (tako da ona na izlazu dade vrijednost 1). U gornjoj tablici to bi bila funkcija \(f_0\).</p>

<div class="mlImportant"><b>Tautologija</b><br>
Booleova funkcija \(f: \mathbb{B}^k \rightarrow \mathbb{B}\) koja svaki element domene preslikava u 1 naziva se <em>tautologija</em>. Tablica istinitosti ove funkcije u posljednjem stupcu ima sve vrijednosti postavljene na 1. Na funkciju možemo gledati i ovako: to je funkcija čija vrijednost ne ovisi o vrijednostima njezinih argumenata - ona je uvijek 1.
</div>

<p>U klasičnoj logici, tautologiju još zovemo i valjanom funkcijom: za svaku moguću kombinaciju ulaza funkcija je zadovoljena pa na izlazu daje vrijednost 1. U gornjoj tablici to bi bila funkcija \(f_3\).</p>

<p>U nastavku su dane tablice istinitosti kontradikcije i tautologije definirane nad dvije varijable.</p>

<table class="mlTableCC">
<tr>
<td style="padding-right: 20px;">
<div class="mlTableCaption"><span class="mlTblKey">Tablica 3.</span> Binarna kontradikcija.</div>
<table class="mlTableCC mlTableVR2">
<thead><tr><th>\(x\)</th><th>\(y\)</th><th>kontradikcija</th></tr></thead>
<tbody>
<tr><td>0</td><td>0</td><td>0</td></tr>
<tr><td>0</td><td>1</td><td>0</td></tr>
<tr><td>1</td><td>0</td><td>0</td></tr>
<tr><td>1</td><td>1</td><td>0</td></tr>
</tbody>
</table>
</td>
<td style="padding-right: 20px;">
<div class="mlTableCaption"><span class="mlTblKey">Tablica 4.</span> Binarna tautologija.</div>
<table class="mlTableCC mlTableVR2">
<thead><tr><th>\(x\)</th><th>\(y\)</th><th>tautologija</th></tr></thead>
<tbody>
<tr><td>0</td><td>0</td><td>1</td></tr>
<tr><td>0</td><td>1</td><td>1</td></tr>
<tr><td>1</td><td>0</td><td>1</td></tr>
<tr><td>1</td><td>1</td><td>1</td></tr>
</tbody>
</table>
</td>
</tr>
</table>

<p>Prije no što nastavimo dalje, pogledajmo dva primjera.</p>

